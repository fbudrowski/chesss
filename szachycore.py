#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-



def infield(position): #finds if the place is in the field
	#print(xd);
	for coordinate in position:
	#	print(i);
		if coordinate < 0 or coordinate > 7:
			return False
	return True

def rotatepos(x):
	a, b = x
	a, b = 7-a, 7-b
	return (a, b)
	#x[0]=7-x[0];x[1]=7-x[1];
	#return x;
def rotatemove(x):
	a, b, c = x
	a, b = rotatepos(a), rotatepos(b)
	return (a, b, c)
	#x[0]=rotatepos(x[0]);x[1]=rotatepos(x[1])
	#return x;
"""
def promoFigToNum(figure):#1 queen 2 bishop 3 knight 4 rook
	if figure=='Q':
		return 1;
	elif figure=='B':
		return 2;
	elif figure=='N':
		return 3;
	elif figure=='R':
		return 4;
	else:
'		return 0;
"""

import copy


class Field:
	import copy
	gameBoard = []							# ar -> gameBoard
	kingMoved = [False, False]				# kingmoved -> kingMoved
	zeroTowerMoved = [False, False]			# qtmoved -> zeroTowerMoved
	sevenTowerMoved = [False, False]		# ktmoved -> sevenTowerMoved
	nextTeam = 0 							# nextteam -> nextTeam (0-uppercase, 1 - lowercase)
	moves = [((0, 0), (0, 0), 0)] 			#new move system - triple:
											#0, 1 - old position, new position, 2 - promo

	#MOVES SUCCESSFULLY REDESIGNED

	def __init__(self):
		self.gameBoard = [
			['r', 'n', 'b', 'q', 'k', 'b', 'n', 'r'],
			['p' for i in range(8)],
			[],#2
			[],#3
			[],#4
			[],#5
			['P' for i in range(8)],
			['R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R']
		]

		for i in range(2, 6):
			self.gameBoard[i] = [' ' for j in range(8)]


	def __deepcopy(self):
		result = Field()
		result.gameBoard = copy.deepcopy(self.gameBoard)
		result.kingMoved = copy.deepcopy(self.kingMoved)
		result.zeroTowerMoved = copy.deepcopy(self.zeroTowerMoved)
		result.sevenTowerMoved = copy.deepcopy(self.sevenTowerMoved)
		result.nextTeam = copy.deepcopy(self.nextTeam)
		result.moves = copy.deepcopy(self.moves)
		return result


	def debug_write(self): #piszpole -> debugwrite
		print("Next one:", self.nextTeam, "Field of the game: ")
		for i in range(8):
			print(self.gameBoard[i])
			#print("{:2}".format(field[i][j]) for j in range(8));
		print("King moved:", self.kingMoved)
		print("Qtow moved:", self.zeroTowerMoved)
		print("Ktow moved:", self.sevenTowerMoved)
		print("Move history:", self.moves)


	#Rotation of field
	def rotate(self): #obrot->rotate
		#print("Reversing");
		self.gameBoard.reverse()			#reverse rows
		for x in range(8):
			self.gameBoard[x].reverse()		#reverse columns
			for y in range(8): 				#change cases
				if self.gameBoard[x][y].isupper():
					self.gameBoard[x][y] = self.gameBoard[x][y].lower()
				elif self.gameBoard[x][y].islower():
					self.gameBoard[x][y] = self.gameBoard[x][y].upper()

		self.kingMoved.reverse()
		self.zeroTowerMoved.reverse()
		self.sevenTowerMoved.reverse()

		self.sevenTowerMoved, self.zeroTowerMoved = self.zeroTowerMoved, self.sevenTowerMoved


		self.nextTeam = 1 - self.nextTeam
		newMoves = []
		for x in self.moves: #TO REDESIGN !!!!!!!!!!!!!!!!!!
			x = rotatemove(x)
			#x=((7-x[0][0],7-x[0][1],x[0][2]),(7-x[1][0],7-x[1][1],x[1][2]));
			newMoves.append(x)
			#print(x);
		self.moves = newMoves

	def get_diffs(self, figure):
		diffsBishop = []
		for i in [-1, 1]:
			for j in [-1, 1]:
				diffsBishop.append((i, j))
		diffsRook = []
		for i in [-1, 1]:
			diffsRook.append((i, 0))
			diffsRook.append((0, i))
		diffs = []
		if figure in ['b', 'B', 'q', 'Q', 'k', 'K']:
			for change in diffsBishop:
				diffs.append(change)
		if figure in ['r', 'R', 'q', 'Q', 'k', 'K']:
			for change in diffsRook:
				diffs.append(change)
		return diffs


	def pawn_chking(self, pos):
		p0, p1 = pos
		for k in [-1, 1]:
			#print("p",p0+1,p1+k,infield((p0+1,p1+k)),
			#self.gameBoard[p0+1][p1+k] if infield((p0+1,p1+k)) else "Poza");
			if (infield((p0+1, p1+k))
				and self.gameBoard[p0+1][p1+k] == "K"):
					return True
		return False
	def knight_chking(self, pos):
		p0, p1 = pos
		diffs = []
		for i in [-2, 2]:
			for j in [-1, 1]:
				diffs.append((i, j))
				diffs.append((j, i))
		for change in diffs:
			i = p0+change[0]
			j = p1+change[1]
			if infield((i, j)) is False:
				continue
			#print("n", xd[0],xd[1],i,j,self.gameBoard[i][j]);
			if self.gameBoard[i][j] == 'K':
				return True
		return False

	def king_chking(self, pos):
		p0, p1 = pos
		for i in range(-1, 2):
			for j in range(-1, 2):
				if infield((p0+i, p1+j)) is True:
					#print("k",i,j,self.gameBoard[p0+i][p1+j]);
					if self.gameBoard[p0+i][p1+j] == 'K':
						return True
		return False

	def figure_chking(self, pos, figure):
		p0, p1 = pos
		chg = self.get_diffs(figure)
		#print(fig, chg);
		for i, j in chg:
			x = p0+i; y = p1+j
			while True:
				if infield((x, y)) is False:
					break
				#print(fig,x,y,self.gameBoard[x][y]);
				if self.gameBoard[x][y] == 'K':
					return True
				if self.gameBoard[x][y] != ' ':
					break
				x = x+i
				y = y+j
		return False



	#Finds if there is a check against
	#the uppercase (black) team by something at pos
	def is_checking_at(self, pos):
		p0, p1 = pos
		#if infield(pos) is False:
		#	return False;
		print(p0, p1, self.gameBoard[p0][p1])
		fig = self.gameBoard[p0][p1]
		if not fig.islower():
			return False
		#print("CHECKING?", p0,p1,fig);
		if fig == 'p':
			return self.pawn_chking(pos)
		elif fig == 'n':
			return self.knight_chking(pos)
		elif fig == 'k':
			return self.king_chking(pos)
		elif fig == 'b' or fig == 'r' or fig == 'q': #bishop, rook, queen
			return self.figure_chking(pos, fig)
		return False


	#Look for any check against party
	def undercheck(self, party):#0 - against uppercase, 1 - against lowercase
		if party == 1:
			self.rotate()
		check = False
		for i in range(8):
			for j in range(8):
				if self.is_checking_at((i, j)):
					check = True
					break
			if check == True:
				break

		if party == 1:
			self.rotate()
		return check







	def isok_move_pawn(self, oldpos, newpos, promo): 		#Moving pawn. Supposing uppercase
		o0, o1 = oldpos
		n0, n1 = newpos
		if (infield((o0, o1)) is False) or (infield((n0, n1)) is False):
			return False

		print(
			"isok_move_pawn from", o0, o1, "to", n0, n1,
			"oldpos", ord(self.gameBoard[o0][o1]),
			"newpos", ord(self.gameBoard[n0][n1]),
			"midpos", ord(self.gameBoard[n0+1][n1])
		)

		if self.gameBoard[n0][n1].isupper():
			return 0
		if n0 == 0 and promo not in ['B', 'Q', 'R']:
			return 0
		if n0 != 0 and promo != 0:
			return 0												#CORRECTNESS CHECK

		if n0 == o0 - 1:											#move 1 step up (forward)
			if n1 == o1:
				return "OK" if self.gameBoard[n0][n1] == ' ' else 0

			elif n1 == o1-1 or n1 == o1+1: 							#one step right or left
				if self.gameBoard[n0][n1].islower(): 				#normal beating (1 forward, 1 left/right)
					return "OK"


				print(len(self.moves)-1)
				print(self.moves[len(self.moves)-1])
				opp, npp, promo = self.moves[len(self.moves)-1]
				print("EP CHK: ", opp, npp, "(", o0, o1, n0, n1, ")")#en passant (status 2)
				if (
					opp == (n0-1, n1) and
					npp == (n0+1, n1) and
					self.gameBoard[n0+1][n1] == 'p'):
						print("EP OK")
						return "EP"
			return 0

		elif o0 == 6 and n0 == o0-2 and n1 == o1:					#skok o 2
			return "OK" if self.gameBoard[n0][n1] == ' ' and self.gameBoard[n0+1][n1] == ' ' else 0
		return 0


	def isok_move_knight(self, oldpos, newpos):		#Moving knight. Supposing uppercase.
		o0, o1 = oldpos
		n0, n1 = newpos
		print("Moving", o0, o1, "to", n0, n1, self.gameBoard[o0][o1], self.gameBoard[n0][n1])
		#if n3!=0:	return 0;
		if self.gameBoard[n0][n1].isupper():
			return 0								#SPECIAL IFS
		diffs = (abs(n0-o0), abs(n1-o1))
		print(diffs)
		if diffs == (2, 1) or diffs == (1, 2):
			return "OK"
		return 0

	def isok_move_figure(self, oldpos, newpos, figure):	#Moving rook, bishop or queen
		o0, o1 = oldpos
		n0, n1 = newpos
		if self.gameBoard[n0][n1].isupper():# or n3 != 0:
			return 0

		changes = self.get_diffs(figure)
		print("XD1")
		for changex, changey in changes:
			x = o0; y = o1
			while True:
				x = x+changex; y = y+changey
				if infield((x, y)) is False:
					break

				print("CHK ", changex, changey, "p", x, y, self.gameBoard[x][y])
				if self.gameBoard[o0][o1].islower() and self.gameBoard[x][y].islower():
					break
				if self.gameBoard[o0][o1].isupper() and self.gameBoard[x][y].isupper():
					break
				if (x, y) == (n0, n1):
					return "OK"
				if self.gameBoard[x][y] != ' ' or figure == 'K' or figure == 'k':
					break
		return 0


	def isok_castling(self, oldpos, newpos):
		o0, o1 = oldpos
		n0, n1 = newpos
		print("TRY CASTLING", o0, o1, n0, n1)
		print("TOWERS MOVED: ZERO", self.zeroTowerMoved, "SEVE", self.sevenTowerMoved)
		if self.undercheck(0) or o0 != n0 or o0 != 7 or self.kingMoved[1] == False:
			return 0
		if n1 == o1-2 and self.gameBoard[o0][0] == 'R':						#zeroside castling
			print("ZERO TOWER CASTLING", self.zeroTowerMoved[1], self.gameBoard[o0][1:o1])
			if self.zeroTowerMoved[1] == True:
				return 0
			if self.gameBoard[o0][1:o1] not in [[' ', ' ', ' '], [' ', ' ']]:
				return 0							#if there's sth between

			self.gameBoard[o0][o1-1] = 'K'; self.gameBoard[o0][o1] = ' '
			ok = self.undercheck(0)
			self.gameBoard[o0][o1-1] = ' '; self.gameBoard[o0][o1] = 'K'
			if ok == False:
				return "CASTLING"
			return 0
		elif n1 == o1+2 and self.gameBoard[o0][7] == 'R':					#sevenside castling
			#print("KS CASTL",self.gameBoard[o0][5:7]);
			print("SEVEN TOWER CASTLING", self.sevenTowerMoved[1], self.gameBoard[o0][o1+1:7])
			if self.sevenTowerMoved[1] == True:
				return 0
			if self.gameBoard[o0][o1+1:7] not in [[' ', ' '], [' ', ' ', ' ']]:
				return 0
			#print("OK");
			self.gameBoard[o0][o1+1] = 'K'; self.gameBoard[o0][o1] = ' '
			ok = self.undercheck(0)
			self.gameBoard[o0][o1+1] = ' '; self.gameBoard[o0][o1] = 'K'
			if ok == False:
				return "CASTLING"
			return 0
		return 0


	def isok_move_king(self, oldpos, newpos):		#Moving king.
		o0, o1 = oldpos
		n0, n1 = newpos
		#if n3!=0:
		#	return 0;
		if self.gameBoard[n0][n1].isupper():
			return 0
		#print("KING",o0,o1,n0,n1);
		if abs(o1-n1) <= 1 and abs(o0-n0) <= 1 and (o0 != n0 or o1 != n1): 		# SHORT MOVE
			return "OK"

		return self.isok_castling(oldpos, newpos)


	def catch_rook_moves(self): #setforcastl -> catch_rook_moves
		if self.gameBoard[0][7] != 'r':
			self.sevenTowerMoved[0] = True
		if self.gameBoard[0][0] != 'r':
			self.zeroTowerMoved[0] = True
		if self.gameBoard[7][7] != 'R':
			self.sevenTowerMoved[1] = True
		if self.gameBoard[7][0] != 'R':
			self.zeroTowerMoved[1] = True

	def do_normal_move(self, o0, o1, n0, n1, promo):
		val = self.gameBoard[o0][o1]
		if val == "K":
			self.kingMoved[1] = True
		self.gameBoard[n0][n1] = val
		self.gameBoard[o0][o1] = ' '
		if promo != 0:
			self.gameBoard[n0][n1] = promo

	def do_enpassant_move(self, o0, o1, n0, n1):
		self.gameBoard[o0][o1] = ' '
		self.gameBoard[n0][n1] = 'P'
		self.gameBoard[n0+1][n1] = ' '
	def do_castling_move(self, o0, o1, n0, n1):
		self.gameBoard[o0][o1] = ' '
		self.gameBoard[n0][n1] = 'K'
		self.kingMoved[1] = True
		if n1 > o1:
			self.gameBoard[n0][n1-1] = 'R'
			self.gameBoard[7][7] = ' '
		else:
			self.gameBoard[n0][n1+1] = 'R'
			self.gameBoard[7][0] = ' '


	def do_move(self, oldpos, newpos, promo, status):
		o0, o1 = oldpos
		n0, n1 = newpos
		print("Doing move from", oldpos, "to", newpos, status)
		self.debug_write()
		if status == 1 or status == "OK":					#Normal good move
			self.do_normal_move(o0, o1, n0, n1, promo)
		elif status == 2 or status == "EP":					#en passant
			self.do_enpassant_move(o0, o1, n0, n1)
		elif status == 3 or status == 4 or status == "CASTLING":#castling
			self.do_castling_move(o0, o1, n0, n1)
		print("Done")
		self.debug_write()
		self.nextTeam = 1-self.nextTeam
		self.moves.append((oldpos, newpos, promo))
		self.catch_rook_moves()
		#print("MOVE",oldpos,newpos,"\nlist:",self.moves);

	def verify_move_without_check(self, oldpos, newpos, promo):
		o0, o1 = oldpos
		n0, n1 = newpos
		ok = True
		if self.gameBoard[o0][o1] == 'P': # pawn
			ok = self.isok_move_pawn((o0, o1), (n0, n1), promo)
		elif self.gameBoard[o0][o1] == 'R': #rook
			ok = self.isok_move_figure((o0, o1), (n0, n1), 'R')
		elif self.gameBoard[o0][o1] == 'N': #knight
			ok = self.isok_move_knight((o0, o1), (n0, n1))
		elif self.gameBoard[o0][o1] == 'B': #bishop
			ok = self.isok_move_figure((o0, o1), (n0, n1), 'B')
		elif self.gameBoard[o0][o1] == 'Q': #queen
			ok = self.isok_move_figure((o0, o1), (n0, n1), 'Q')
		elif self.gameBoard[o0][o1] == 'K': #king
			ok = self.isok_move_king((o0, o1), (n0, n1))
		else:
			ok = False
		return ok


	def verify_move(self, oldpos, newpos, promo=0):#checks IF a move is correct (including possible check), returns True/False
		#try:
			o0, o1 = oldpos
			n0, n1 = newpos
			if o0 == n0 and o1 == n1: return False
			hasBeenReversed = False
			print(o0, o1, n0, n1)
			print(infield((o0, o1)), infield((n0, n1)))
			if (infield((o0, o1)) is False) or (infield((n0, n1)) is False):
				print("Invalid move: one of fields outside the game area")
				return False
			#print("XD-1");
			if (not self.gameBoard[o0][o1].islower()) and (not self.gameBoard[o0][o1].isupper()):
				print("Invalid move: empty field selected.")
				return False
			supportField = copy.deepcopy(self)
			if supportField.gameBoard[o0][o1].islower(): # REVERSE
				hasBeenReversed = True
				print("O1")
				supportField.rotate()
				o0, o1 = rotatepos((o0, o1))
				n0, n1 = rotatepos((n0, n1))
				#o0=7-o0;o1=7-o1;n0=7-n0;n1=7-n1;

			#supportField.debug_write();self.debug_write();
			print("OK1", supportField.gameBoard[o0][o1]) #I CAN DEEPCOPY CLASSES YAY



			ok = supportField.verify_move_without_check((o0, o1), (n0, n1), promo)
			#OK STATUS:  0/False - invalid move, 1/True/"OK" - valid move, 2/"EP" - en passant
			# 3/4/"CASTLING" - castling.
			print("OK", ok)


			if ok != False and ok != 0 and ok != None:
				supportField.do_move((o0, o1), (n0, n1), promo, ok)
				if supportField.undercheck(0):
					print("Invalid move - check")
					ok = 0
			if hasBeenReversed==True:
				print("O2")
				supportField.rotate()
				o0, o1 = rotatepos((o0, o1))
				n0, n1 = rotatepos((n0, n1))
				#o0=7-o0;o1=7-o1;n0=7-n0;n1=7-n1;
			if ok == False or ok == 0 or ok == None:
				return False
			return supportField
		#except:
		#	print("ERROR");
	def get_pawn_moves(self, oldpos):
		o0, o1 = oldpos
		ans = []
		listMoves = []
		for i in [-1, 0, 1]:
			for j in [0, 'Q', 'B', 'N']:
				listMoves.append((o0-1, o1+i, j));
		listMoves.append((o0-2, o1, 0));
		print("LMLM", listMoves)
		for newpos in listMoves:
			n0,n1,promo=newpos;
			if (infield((n0,n1)) 
			and self.isok_move_pawn((o0, o1), (n0, n1), promo) not in [0,False,None]
			and self.verify_move((o0, o1), (n0, n1), promo) not in [0,False, None]):
				ans.append(newpos);
		return ans;
	
	def get_king_moves(self,oldpos):
		o0, o1 = oldpos;
		ans=[];
		listmoves=[];
		for i in [-1,0,1]:
			for j in [-1,0,1]:
				if i!=0 or j!=0:
					listmoves.append((o0+i,o1+j,0));
		listmoves.append((o0,o1-2,0));
		listmoves.append((o0,o1+2,0));
		for newmove in listmoves:
			newpos=(newmove[0],newmove[1])
			if (infield(newpos) 
			and self.isok_move_king((o0,o1),newpos)not in [0,False, None]
			and self.verify_move((o0,o1),newpos,0)not in [0,False, None]):
				ans.append(newmove);
		return ans;
		
	def get_knight_moves(self,oldpos):
		o0, o1 = oldpos;
		ans=[];
		listmoves=[];
		for i in [-2,-1,1,2]:
			for j in [-2,-1,1,2]:
				if abs(i) == abs(j):
					continue;
				if infield((o0+i,o1+j)) is False:
					continue;
				listmoves.append((o0+i,o1+j,0));
		for newmove in listmoves:
			newpos=(newmove[0],newmove[1])
			if (infield(newpos)
			and self.isok_move_knight((o0,o1),newpos) not in [0,False,None] 
			and self.verify_move((o0,o1),newpos,0) not in [0,False,None]):
				ans.append(newmove);
		return ans;
		
	def get_figure_moves(self,oldpos,figure):
		o0, o1 = oldpos;
		ans=[];
		for i in [-1,0,1]:
			for j in [-1,0,1]:
				if i==0 and j==0:
					continue;
				x=o0+i;y=o1+j;
				while True:
					if infield((x,y)) is False:
						break;
					if self.isok_move_figure((o0,o1),(x,y),figure)==0:
						break;
					if self.verify_move((o0,o1),(x,y))!=False:
						ans.append((x,y,0));
					x=x+i;y=y+j;
		return ans;
		
	def seemoves(self,oldpos):
		o0, o1 = oldpos;
		if infield(oldpos)==False:
			return [];
		if (self.gameBoard[o0][o1].islower()==False) and (self.gameBoard[o0][o1].isupper()==False):
			#print("Wybrane puste pole.");
			return [];
		supportField=copy.deepcopy(self);
		hasBeenReversed=False;
		if supportField.gameBoard[o0][o1].islower():
			hasBeenReversed=True;
			o0,o1=rotatepos((o0,o1));
			#o1=7-o1;o2=7-o2;
			print("O1");
			supportField.rotate();
		print("SEEMOVES",o0,o1,self.gameBoard[o0][o1],supportField.gameBoard[o0][o1]);
		ans=[];
		if supportField.gameBoard[o0][o1]=='P':
			ans=supportField.get_pawn_moves((o0,o1));
		elif supportField.gameBoard[o0][o1]=='K':
			ans=supportField.get_king_moves((o0,o1));
		elif supportField.gameBoard[o0][o1]=='N':
			ans=supportField.get_knight_moves((o0,o1));
		elif supportField.gameBoard[o0][o1] in ['Q','B','R']:
			ans=supportField.get_figure_moves((o0,o1),supportField.gameBoard[o0][o1]);
		else:
			ans=[];
		if hasBeenReversed==True:
			supportField.rotate();
			o0,o1=rotatepos((o0,o1));
			#o1=7-o1;o2=7-o2;
			print(ans);
			ans=[(7-v[0],7-v[1],v[2]) for v in ans];
			print(ans);
		print("ANS",ans);
		return ans;
		
		
	def checkmate(self):
		hasBeenReversed=False;
		if self.nextTeam == 1:
			hasBeenReversed=True;
			self.rotate();
		
		nowUnderCheck = self.undercheck(0);
		canMove=False;
		print("LOOK FOR CHECKMATE")
		for i in range(7):
			for j in range(7):
				if self.gameBoard[i][j].isupper():
					availableMoves=self.seemoves((i,j))
					print(i,j,self.gameBoard[i][j],availableMoves)
					if len(availableMoves) > 0:
						canMove=True;
						break;
			
			if canMove == True:
				break;
		if hasBeenReversed == True:
			hasBeenReversed = False;
			self.rotate();
		
		if canMove==True:
			return -1;
		if canMove == False:
			if nowUnderCheck == True:
				return (self.nextTeam+1)%2;
			else:
				return "TIE"
		
	
						
	def playthegame(self, oldpos,newpos,promo):
		print(oldpos,newpos);
		o0, o1 = oldpos;
		n0,n1=newpos;
		print("Moving from",o0,o1,self.gameBoard[o0][o1],"to",n0,n1);
		self.debug_write();
		if infield((o0,o1)) is False:
			return False;
		print("A");
		if self.nextTeam==0 and (not self.gameBoard[o0][o1].isupper()):
			return False;
		if self.nextTeam!=0 and (not self.gameBoard[o0][o1].islower()):
			return False;
		print("OK1");
		ans=self.verify_move(oldpos,newpos,promo);
		return ans;
		
	
	def trytoread(self):
		try:
			with open('game.dat') as fle:
				for line in fle:
					print(line);
					wej=line.rstrip(' \n\t');
					print("A",wej);
					a,b,c,d,e,f=wej.split(' ');
					print(a,b,c,d,e,f);
					for xd in [a,b,d,e]:
						print(xd,xd.isdigit());
						if not xd.isdigit():
							print("Bad format!");
							raise Exception();
					a,b,d,e=map(int,[a,b,d,e]);
					c,f=map(promotonum,[c,f]);
					ans=playthegame(self,(a,b,c),(d,e,f));
					if ans==False:
						print("Wrong move!");
						continue;
					else:
						print("Good move!");
						fld=ans;
		except:
			print("End of file!");
		pass;
		
		

		
		
		
def readmove():
	try:
		wej=input();
	except:
		print("End of data");
		return False;
	if wej=='':
		return False;
	a,b,c,d,e,f=wej.split(' ');
	for xd in [a,b,d,e]:
		if not xd.isdigit():
			return False;
	a,b,d,e=map(int,[a,b,d,e]);
	c,f=map(promotonum,[c,f]);
	zm=(a,b,c,d,e,f);
	print(zm);
	return zm;
	
def wholegame():
	fld=Field();
	
	try:
		while True:
			fld.debug_write();
			
			
			zm=readmove();
			a,b,c,d,e,f=zm;
			print("A",a,b,c,d,e,f);
			ans=playthegame(fld,(a,b,c),(d,e,f));
			print("B");
			if ans==False:
				print("Wrong move!");
				continue;
			else:
				print("Good move!");
				fld=ans;
	except:
		print("Eo input");


