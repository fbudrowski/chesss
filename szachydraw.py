#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pygame;
import pygame.locals;
import szachycore as chcore;
import time;
import szachywindow as chwindow;

class GameWindow(object):
	hlted=[];
	def __init__(self, width, height):
		self.boardWidth=width;
		self.boardHeight=height;
		self.surface = pygame.display.set_mode((width,height),0,32);
		pygame.display.set_caption("Game of chess");
		
	def drawrct(self,x,y,color):
		print("x0 {}, x {}, y0 {}, y {}, size {}".format(self.x0,x,self.y0,y,self.fSize));
		xp=self.x0+x*self.fSize;
		yp=self.y0+y*self.fSize;
		pygame.draw.rect(self.surface,color,(xp,yp,self.fSize,self.fSize));
	def drawpitch(self):
		background=(255,255,0);
		self.surface.fill(background);
		size=int(0.75 * float(min(self.boardWidth, self.boardHeight)));
		self.boardSize=size;
		fsize=int(float(size)/float(8));
		self.fSize=fsize;
		print("BOARD: {} X {};; SIZE {}".format(self.boardWidth,self.boardHeight,size));
		xp=int(float(self.boardWidth)/2. - float(size)/2.);
		self.x0=xp;
		yp=int(float(self.boardHeight)/2. - float(size)/2.);
		self.y0=yp;
		for x in range(8):
			for y in range(8):
				if (x+y)%2 == 0:
					color=(255,255,255);
				else: color=(0,0,0);
				self.drawrct(x,y,color);
		pygame.display.update();
		
	def drawtext(self,surface,text,center, color=(180,180,180)):
		print("Drawing {} in place {} color {}".format(text,center,color));
		text=self.font.render(text,True,color);
		rect=text.get_rect();
		rect.center=center;
		surface.blit(text,rect);
		pygame.display.update();
	def drawfig(self,fig,x,y):
		xp=int(self.x0+float(self.fSize)/2.)+x*self.fSize;
		yp=int(self.y0+float(self.fSize)/2.)+y*self.fSize;
		self.drawtext(self.surface,fig,(xp,yp));
	
	def drawfigs(self,fld):
		pygame.font.init();
		font_path = pygame.font.match_font('ubuntu', True);#'arial');
		self.font=pygame.font.Font(font_path, int(float(self.fSize)*0.5));
		size=self.boardSize;
		#xp=int(self.x0+float(self.fSize)/2.);
		for x in range(8):
			self.drawfig(str(chr(ord('a')+x)),x,-1);
			self.drawfig(str(8-x),-1,x);
			self.drawfig(str(chr(ord('a')+x)),x,8);
			self.drawfig(str(8-x),8,x);
			#yp=int(self.y0+float(self.fSize)/2.);
			for y in range(8):
				self.drawfig(fld.gameBoard[y][x],x,y);
				#self.drawtext(self.surface,fld.gameBoard[y][x],(xp,yp));#chcore.tounicode(fld.gameBoard[y][x]),(xp,yp));
				#yp+=self.fSize;
			#xp+=self.fSize;
		pass;
	def highlight(self,fld,x,y,color=0):
		print(type(color), color);
		if type(color)==int and color==0:
			if (x+y)%2 == 0:
				color=(255,255,255);
			else: color=(0,0,0);
		self.drawrct(x,y,color);
		self.drawfig(fld.gameBoard[y][x],x,y);
		
		pygame.display.update();
	
	oldpos=None;newpos=None;
	def playgame(self,fld,x,y):
		if (type(x) is not int) or (type(y) is not int) or x<0 or x>7 or y<0 or y>7:
			self.oldpos=None;
			return False;
		print("PLAYGAME",x,y,self.hlted);
		print(x,y,fld.gameBoard[x][y],self.oldpos,fld.nextTeam);
		fld.debug_write();
		
		for i in self.hlted:
			self.highlight(fld,i[0],i[1]);
		self.hlted=[];
		if self.oldpos!=None:
			self.newpos=(x,y);
			self.highlight(fld,self.oldpos[1],self.oldpos[0]);
			promotion=0;
			if fld.gameBoard[self.oldpos[0]][self.oldpos[1]] in ['p','P']:
				if ((self.newpos[0] in [0,7] )
				and abs(self.oldpos[0]-self.newpos[0])<=2 
				and abs(self.oldpos[1]-self.newpos[1])<=2):
					promoquery=chwindow.PromotionQuery();
					promotion=promoquery.returnAnswer();
					if promotion=="&Queen": promotion='Q'
					if promotion=="K&night": promotion='N'
					if promotion=="&Bishop": promotion='B'
					if promotion=="&Rook": promotion='T'
					print(promotion);
					#promotion=input("How do you want to promote?").upper();
					
			
			ans=fld.verify_move(self.oldpos,self.newpos,promotion);
			#print(ans);
			self.oldpos=None;self.newpos=None;
			if ans!=False:
				fld=ans;
				self.drawpitch();
				self.drawfigs(fld);
				pygame.display.update();
				print(x,y,fld.gameBoard[x][y],self.oldpos,fld.nextTeam);
				return fld;
		else:
			if fld.nextTeam==0 and (not fld.gameBoard[x][y].isupper()):
				return False;
			if fld.nextTeam!=0 and (not fld.gameBoard[x][y].islower()):
				return False;
			self.oldpos=(x,y);
			self.highlight(fld,y,x,(0,255,255));
			self.hlted.append((y,x,0));
			lst=fld.seemoves((x,y));
			for i in lst:
				self.highlight(fld,i[1],i[0],(0,255,255));
				self.hlted.append((i[1],i[0],0));
		
		return False;
		
		
		
		
	
	def handleclicks(self, fld):
		import math;
		hasChanged=False;
		for event in pygame.event.get():
			#print("EV:",event);
			if event.type == pygame.locals.QUIT:
				pygame.quit();
				return True;
			elif event.type==pygame.locals.MOUSEBUTTONDOWN:
				xx,yy=pygame.mouse.get_pos();
				x=float(xx-self.x0)/float(self.fSize);
				y=float(yy-self.y0)/float(self.fSize);
				ans=self.playgame(fld,math.floor(y),math.floor(x));
				if ans==False:
					return True;
				else:
					fld=ans;
					hasChanged=True;
			elif event.type==pygame.locals.KEYDOWN:
				if event.key!=27:
					continue;
				print (event.key);
			else:
				#print("X")
				continue;
		if hasChanged==False:
			return True;
		return fld;
		
	
	
	
	
	

board=GameWindow(700,400);
board.drawpitch();
fld=chcore.Field();
fld.debug_write();
board.drawfigs(fld);

#time.sleep(5);
while True:
	ans=board.handleclicks(fld);
	#print(ans);
	if ans!=True:
		fld=ans;
		blok = fld.checkmate();
		#print(ans,"LOOK FOR CHM");
		#blok=-1;
		if blok!=-1:
			print("Result:",blok);
			break;
	
	"""x,y=input().split();
	x=int(x);y=int(y);
	print(x,y,fld.gameBoard[x][y]);
	ans=board.playgame(fld,x,y);
	if ans!=False:
		fld=ans;"""
	
	#board.highlight(fld,x,y,(0,255,255));
	#time.sleep(3);
	#board.highlight(fld,x,y);


#print("Pygame font {}\npath arial {}\npath default {}".format(pygame.font.get_default_font(),
#pygame.font.match_font('arial'), pygame.font.match_font(pygame.font.get_default_font())));
